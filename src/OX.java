import java.util.Scanner;
public class OX {
	Scanner kb = new Scanner(System.in);
	static char[][]  board = {{' ','1','2','3'},
							  {'1','-','-','-'},
					   		  {'2','-','-','-'},
					   		  {'3','-','-','-'}};
	static char turn = 'x';
	static boolean d = true;
	static String result;
	static void printwelcome() {
		System.out.println("Welcome to OX Game");
	}
	static void printboard() {
		for(int i = 0 ; i < 4;i++) {
			for(int j = 0 ; j < 4;j++) {
				System.out.print(board[i][j]+" ");
			}System.out.println();
		}
	}
	static void switchturn( ) {
		if(turn == 'x' ) {
			turn = 'o';
		}else if(turn == 'o' ) {
			turn = 'x';
		}
	}
	static void printturn() {
		System.out.println("turn "+turn);
	}
	static void input() {
		Scanner kb = new Scanner(System.in);
		int Row;
		int Col;
		while (true) {
			while (true) {
				System.out.println("Please input Row Col :");
				Row = kb.nextInt();
				Col = kb.nextInt();
				if(Row < 1 || Row > 3) {
					System.out.println("Error to input try again");
					continue;
				}else if(Col < 1 || Col > 3)	{
					System.out.println("Error to input try again");
					continue;
				}else {
					break;
				}
			}
			if(board[Col][Row] != '-' ) {
				System.out.println("Error to input try again");
			}else {
				board[Col][Row] = turn;
				break;
			}
		}
	
	}
	static void chackwin() {
		if(board[1][1] == turn&&board[1][2] == turn&&board[1][3] == turn ||
		   board[1][1] == turn&&board[1][2] == turn&&board[1][3] == turn || 
		   board[1][1] == turn&&board[1][2] == turn&&board[1][3] == turn) {
			d = false;
			result = "Win";
		}else if(board[1][1] == turn&&board[2][1] == turn&&board[3][1] == turn ||
				 board[1][2] == turn&&board[2][2] == turn&&board[3][2] == turn || 
				 board[1][3] == turn&&board[2][3] == turn&&board[3][3] == turn) {
			d = false;
			result = "Win";
		}else if(board[1][1] == turn&&board[2][2] == turn&&board[3][3] == turn ||
				 board[1][3] == turn&&board[2][2] == turn&&board[3][1] == turn) {
			d = false;
			result = "Win";
		}else if( board[1][1] == '-' ||board[1][2] == '-' ||board[1][3] == '-' ||
				  board[2][1] == '-' ||board[2][2] == '-' ||board[2][3] == '-' ||
				  board[3][1] == '-' ||board[3][2] == '-' ||board[3][3] == '-' ){
			d = true;
			
		}else {
			d = false;
		result = "Draw";
		}
	}
	static void printwin() {
		if( result == "Win") {
		System.out.println(turn + " "+ result);
		}else {
			System.out.println(result);
		}
		System.out.println("Bye");
		
	}
	

	public static void main(String[] args) {
		printwelcome();
		while(d){
		printboard();
		printturn();
		input();
		chackwin();
		if(d == true) {
			switchturn();
		}
		}
		printboard();
		printwin();
		
		
		

	}
	
	
	

}
